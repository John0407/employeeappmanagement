package com.example.employeemanagementsystem.utils;

public interface Response {
	
	String ERROR_MESSAGE_NO_VALID_DETAILS = "Please enter valid details";
	String SUCCESS_CODE = "Success";
	String ERROR_MESSAGE_ALREADY_EXISTS = "The Employee is already registered";
	String ERROR_MESSAGE_INVALID_ACCESS = "Please swipe out before swipe in again";
	String ERROR_MESSAGE_FORBIDDEN_ACCESS = "Only Admin has the Access";

}
