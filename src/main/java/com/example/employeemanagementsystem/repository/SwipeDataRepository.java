package com.example.employeemanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.employeemanagementsystem.entity.SwipeData;

public interface SwipeDataRepository extends JpaRepository<SwipeData, Long> {

}
